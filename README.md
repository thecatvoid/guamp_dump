# msi-user 10 QP1A.191005.002 e3498 release-keys
guamp_retail-user 10 QPX30.30-Q3-38-69-1 e3498 release-keys
- manufacturer: 
- platform: bengal
- codename: guamp
- flavor: msi-user
- release: 10
- id: QP1A.191005.002
QPX30.30-Q3-38-69-1
- incremental: e3498
- tags: release-keys
- fingerprint: motorola/guamp_retail/guamp:10/QPX30.30-Q3-38-69-1/e3498:user/release-keys
- is_ab: true
- brand: motorola
- branch: msi-user-10-QP1A.191005.002-e3498-release-keys
guamp_retail-user-10-QPX30.30-Q3-38-69-1-e3498-release-keys
- repo: motorola_guamp_dump
